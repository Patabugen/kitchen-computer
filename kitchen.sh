#!/bin/bash
echo "Welcome to Kitchen Komputer"

export cmd=$1

# Move into the cmd folder
cd /usr/local/bin/kitchen-computer/cmd

function doKitchenStuff {
	clear;
	# Get the next bit of the command
	export thisOne=${1:0:1}
	export cmd=${1:1}
	export newDir="";
	
	# List all the files for our users enjoyment, also check for a match which we automatically choose.
	echo "Your options are: "
	for f in *
	do
		echo "		$f"
		export firstChar=${f:0:1}
		if [ "$thisOne" == "$firstChar" ]; then
			export newDir=$f
		fi;
	done
	
	# If we found no match, ask for user input
	if [ -z "$newDir" ]; then
		echo "Pick a command from above"
		read cmd
		doKitchenStuff $cmd
		return
	fi

	# If we found a directory, CD into it and carry on. If it's a file, run it.
	if [ -d "$newDir" ]; then
		cd "$newDir";
		doKitchenStuff $cmd
	else
		echo "Run: $newDir from `pwd`"
		clear
		bash "$newDir"
		return;
	fi
	return;
}

doKitchenStuff $cmd
